# Vehicle Detection Project

Submission by: Swapnil Devikar

----

## The goals / steps of this project are the following:

* Perform a Histogram of Oriented Gradients (HOG) feature extraction on a labeled training set of images and train a classifier Linear SVM classifier
* Optionally, you can also apply a color transform and append binned color features, as well as histograms of color, to your HOG feature vector.
* Note: for those first two steps don't forget to normalize your features and randomize a selection for training and testing.
* Implement a sliding-window technique and use your trained classifier to search for vehicles in images.
* Run your pipeline on a video stream (start with the test_video.mp4 and later implement on full project_video.mp4) and create a heat map of recurring detections frame by frame to reject outliers and follow detected vehicles.
* Estimate a bounding box for vehicles detected.

[//]: # (Image References)
[image1]: ./output_images/car_not_car.png
[image2]: ./output_images/hog_extracted_car.png
[image3]: ./output_images/hog_extracted_noncar.png
[image4]: ./output_images/sliding_window.png
[image5]: ./output_images/bboxes_and_heat.png
[image6]: ./output_images/labels_map.png
[video1]: ./output_project_video.mp4

## Submission content:
* `P5.ipynb`: Main project notebook in which vehicle detection is performed first on test images and the video stream
* `Preprocessing and Training SVC.ipynb` : Notebook in which input data validation is preformed and various processing options are explored. Also, training of SVC classifier is done here.
* `helper_functions.py`: Seperate file with helper functions are implemented (these are mostly the functions discussed in lessons)
* `tracker.py`: Seperate class that keeps track of cars and their binding boxes
* `svc_pickle/`: Directory where trained classifier is pickled
* `output_images/`: Reference output images are stored here
* `output_project_video.mp4`: Final output of the project

## [Rubric](https://review.udacity.com/#!/rubrics/513/view) Points
###Here I will consider the rubric points individually and describe how I addressed each point in my implementation.  

---
###Writeup / README

####1. Provide a Writeup / README that includes all the rubric points and how you addressed each one.  You can submit your writeup as markdown or pdf.  [Here](https://github.com/udacity/CarND-Vehicle-Detection/blob/master/writeup_template.md) is a template writeup for this project you can use as a guide and a starting point.  

You're reading it!

### Histogram of Oriented Gradients (HOG)

#### 1. Extracting HOG features from the training images.

The code for this step is contained in the code cells below the "Single Image Feature Extraction" header of the IPython notebook `Preprocessing and Training SVC.ipynb` (In[4])
In this cell, I have done a side by side comparison of how hog features appear for a car image versus a non car image. It is easily observable here that the hog features do accentuate certain structural properties of a car. e.g. the license plate, wheels etc.
For example, following image shows a side by side comparison of car and a non car image
![alt text][image1]

After extracting hog features, the side by side comparison for a car image looks as follows:
![alt text][image2]

After extracting hog features, the side by side comparison for a non car image looks as follows:
![alt text][image3]

I did several iterations of the above step, each time grabbing a random image and observing how features are extracted. One observation here was the properties of structural parameters have one to one relationship with the orientations and their magnitude in the hog features. e.g. the length and width of car can be judged from HOG representation. The change in the lighting in the original image is also observable.

The next step was to experiment with different color spaces. I started with training the SVC classifier and see which color space results in better accuracy, while keeping the other parameters same.

#### 2. Settling on final choice of HOG parameters.
In order to determine the parameters to use for feature extraction, I decided to see how the SVC classifier performs for different parameters. I selected a random list of 1000 images with and without cars, with the help of the following helper function.
`get_random_list(type='both', nsamples = 1000)
`
And trained SVC by starting with some sensible selection of parameters. e.g. selecting channel 0 for HOG, orient = 9, number of bins = 9 and so on.
The idea behind this was to see how SVC performs for each of the parameters and classifies the images more accurately.
I tried various combinations of color spaces, orientations, histograms bins and hog channels. I recorded the results as I went along in the notebook itself for easy reference. In this experimentation, two color spaces particularly stood out. `HSV` and `YCrCb`. Both color spaces yielded > 99% accuracy with a subsample of 1000 random images. In general, accuracy was better when all 3 HOG channels were used, as against a single HOG channel. I documented these test results as I went along with changing the test parameters.
At the end, I wrote a helper method that returned these fixed parameters:
` get_feature_extraction_params()
`
This helper function would be used to get the parameters for extracting the features going forward

#### 3. Training the classifier using selected HOG and color features
When the parameters were finalized, I trained the classifier once with the entire dataset of images.
This is done in In [6] in notebook `Preprocessing and Training SVC.ipynb`, below header *Training with entire dataset*
The trained classifier and feature scalars are pickled for later use, so we don't have to repeat this time consuming process later. The pickling is done in the cell marked In [18], below header *Save the training results for later use*  

### Sliding Window Search

#### 1. Implementation a sliding window search. And selection of scales to search and overlap proportions
I separated the code for this part of the project into `P5.ipynb`
This was done in three steps.
**Testing of trained classifier with individual feature extraction**:
The code for this part is in `P5.ipynb` notebook in cell In[4], below header `Testing on test images`
The intent of extracting features individually, i.e. without HOG subsampling was to test the SVC and see what parameters work best for sliding window algorithm, without adding additional complexities.
Looking at the video and test images, it was clear that the cars will only appear in a certain portion of the image along y axis. So, with visual inspection, the search for cars was limited to the range 400-656 along y axis.
Then I tried two different values of parameters as follows:
window overlap: 0.5, 0.75
xy_window: 64, 128 and 96
Other parameters were kept the same as they were already tested and known to yield good results.
One of the goals in selection of window size and overlap percent was to keep the number of search windows as low as possible for faster processing and the size of window to be proportionate to the feature we're trying to spot in the image. With this in mind, in terms of the size of the feature, window size = 96 sounded like a good compromise between 64 and 128 and its also the multiple of the size of the input images. Overall, I tried to keep the number of windows within the range of few hundreds and less than 1000. With 75% overlap and window size = 96, I was able to detect all cars in all 6 test images and have very few false positives at the same time. This is the result at the end of this step

![alt text][image4]

**Repeat the process with HOG subsampling**
This part is implemented in `P5.ipynb` cell In[5], under the header `Extracting features with Hog subsampling`
The goal of this step was to obtain similar results as above, but faster, using HOG subsampling.
Because I was able to get great results with the parameters above, I selected the parameters for this step, such that they effectively match with the parameters selected in the first step.
e.g.
- for 8x8 cell size, cells_per_step = 2 will mean 75% overlap in sliding windows.
- window size was also set to 96x96
- The exact same portion of image was searched using sliding windows (i.e. y = 400 to y = 656)
- since 16x16 spatial size worked just fine before, the scale was set to 1

I used the exact same approach as described in the lessons to extract features and apply sliding window algorithms to spot the cars in the image. The function that performs this job is:
`find_cars(img, ystart, ystop, scale, svc, X_scaler, xy_window, window=64)`
This function takes an input image and extracts `hog_features`, `spatial_features` and `hist_features` and then scales and appends them together using `np.hstack`.
The function the uses trained classifier to predict whether the image contains cars or not and if it does contain cars, returns an image with rectangles drawn on it along with the coordinates for the drawn rectangle and the heatmap.
The idea behind the heatmap is to keep track of the pixel coordinates where the cars were detected earlier. We keep track of these areas, by maintaining a map with the same width and height as the image we're processing and then keep on adding 1s to the portions of image where the features are detected.
We can use this to get rid of some sporadic detections by thresholding the heatmap. The `apply_threshold` function was written for this:

`heat = apply_threshold(heat,10)
`
An example of what a heatmap for an image with two cars looked like, is shown below
![alt text][image5]

In the notebook, I have shown a side by side comparison of all the test images with rectangles drawn around them to the left and the corresponding heatmap image to the right. Please refer to In[13] cell output for this.

Ultimately I searched using YCrCb 3-channel HOG features plus spatially binned color and histograms of color in the feature vector, which provided a nice result.  

As discussed above, the heatmap technique was used to get rid of false positives and avoid multiple detections


---

### Video Implementation

####1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (somewhat wobbly or unstable bounding boxes are ok as long as you are identifying the vehicles most of the time with minimal false positives.)
Here's a [link to my video result](./output_project_video.mp4)


#### 2. Describe how (and identify where in your code) you implemented some kind of filter for false positives and some method for combining overlapping bounding boxes.

I recorded the positions of positive detections in each frame of the video.  From the positive detections I created a heatmap and then thresholded that map to identify vehicle positions.  I then used `scipy.ndimage.measurements.label()` to identify individual blobs in the heatmap.  I then assumed each blob corresponded to a vehicle.  I constructed bounding boxes to cover the area of each blob detected.
I also implemented a tracker, in `tracker.py` that maps the cars to the recently found bounding boxes. In order to avoid wobbly boxes, it provides a method, `get_best_fit_bbox()` which averages a position of bounding box over the last n frames.
Since, once the car appears in the image, it can't just vanish, the history is tracked, even though the car is not detected in last n frames. The result, I was hoping to get here, is a bounding box, whose size decreases as the size of the car decreases in subsequent frames.

Here's an example result showing the heatmap from a series of frames of video, the result of `scipy.ndimage.measurements.label()` and the bounding boxes then overlaid on the last frame of video:

### Here is the output of `scipy.ndimage.measurements.label()` on the integrated heatmap from all six frames:
![alt text][image6]

---

###Discussion

####1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

Overall approach taken to come up with a solution for this project was almost exactly as it was discussed in the class. I wanted to make additions to the code, where I can track the cars and possibly extrapolate their position as they move. The `tracker` class was implemented for this purpose. This class was designed to maintain a dictionary of car numbers and recent n bounding boxes for a car. I implemented a logic here to do the following:
- When a car is detected in a frame, register it in a 'database' and record its bounding boxes
- Instead of drawing a bounding box at a position reported by the heatmap, draw the bounding box over the positions that is averaged over the last n iterations
- When the same car is not present in a subsequent frame, (which might be a sign that the classifier has failed to detect the car in the image), assume that the classifier miscategorized the image and draw a bounding box that is averaged over the last n box positions for that car. But do this only x number of times. i.e. if the car is missing from the frame for more than `smoothing threshold` times, assume that it really went out of the frame and start drawing the bounding box.
I will discuss below as to why this approach failed.
This approach works well when there's a single car in the frame and it's being properly detected by the classifier. However, when there are more than one cars and in the second car ends up getting in the first car's position, there will be multiple boxes drawn on a second car. This was undesirable, so I removed this behavior by setting the default smoothing threshold to 1.

Another challenge that I faced was seeing a different behavior of classifier when feature extraction is done using subsampling vs when features are extracted individually. As seen in the `sliding_window.png` image, the white car is properly detected in the images, but the same car is not detected in the video, with hog subsampling. The way to fix this, is to extract features from more color spaces and train SVC, which will be an improvement.
