import numpy as np
from collections import deque
import numpy as np

class tracker():
    def __init__(self, nrecords=20, smoothing_threshold = 50):
        print('tracker initialized')        
                
        # dictionary that maps cars to their recent bboxes key = car, value = deque of recent bboxes
        self.cars_to_recent_bboxes_map = {}
        
        # keep track of when detections go missing for an exisitng car
        self.cars_to_no_detection_counter_map = {}
        
        # these are the number of bounding boxes for which, we will maintain history
        self.nrecords = nrecords
        
        # list of detected car numbers so far
        self.known_carnumbers = set()
        
        # smoothing factor
        self.smoothing_threshold = smoothing_threshold
    
    def record_last_position(self, car, bbox):
        if  car in self.cars_to_recent_bboxes_map :
            #print("car {} already being tracked. Appending bbox".format(car))
            bbox_deque = self.cars_to_recent_bboxes_map[car]
            bbox_deque.append(bbox)
        else:
            #print("car{} doesn't exist. creating car".format(car))
            bbox_deque = deque((), self.nrecords)
            bbox_deque.append(bbox)
            self.cars_to_recent_bboxes_map[car] = bbox_deque
    
    def register_missing_detection(self, missing_car):
        if missing_car in self.cars_to_no_detection_counter_map:
            miss_count = self.cars_to_no_detection_counter_map[missing_car]
            self.cars_to_no_detection_counter_map[missing_car] = miss_count + 1
        else:
            self.cars_to_no_detection_counter_map[missing_car] = 0
        
    def check_for_missing_detections(self, bboxes_carnumbers):
        carnumbers_in_frame = []
        for bbox, carnumber in bboxes_carnumbers:
            # list of carnumbers we got in this frame
            carnumbers_in_frame.append(carnumber)
            
        for known_car in self.known_carnumbers:
            if known_car not in carnumbers_in_frame:
                # check if this car has been missing already
                self.register_missing_detection(known_car)
                
                    
        
    def track(self, bboxes_carnumbers):
        #print('tracking...')
        self.check_for_missing_detections(bboxes_carnumbers)
        
        for bbox, carnumber in bboxes_carnumbers:
            #print("car number:", carnumber, "bounding box", bbox)
            self.record_last_position(carnumber, bbox)
            self.known_carnumbers.add(carnumber)
            
    def get_best_fit_bbox(self):
        bboxes_carnumbers = []
        for carnumber in self.cars_to_recent_bboxes_map:
            #print('getting best fit box for carnumber:', carnumber)
            #print('current bbox:', self.cars_to_recent_bboxes_map[carnumber])
            if carnumber in self.cars_to_no_detection_counter_map:
                # how long has this car been missing
                missing_counter = self.cars_to_no_detection_counter_map[carnumber]
                if missing_counter > self.smoothing_threshold :
                    #print("hit the missing counter")
                    # reset the missing counter for this car
                    self.cars_to_no_detection_counter_map[carnumber] = 0 # or decrease the counter
                    # replace the bounding box with empty deque, and in effect resetting the tracking
                    self.cars_to_recent_bboxes_map[carnumber].clear()
                else:
                    #print("before averaging:", self.cars_to_recent_bboxes_map[carnumber])
                    if len(self.cars_to_recent_bboxes_map[carnumber]):
                        bbox = np.average(self.cars_to_recent_bboxes_map[carnumber], axis = 0).astype(np.int32)
                        #print("after averaging:", self.cars_to_recent_bboxes_map[carnumber])
                        bbox = tuple(map(tuple, bbox))
                        bboxes_carnumbers.append((bbox, carnumber))
            else:    
                #print("before averaging:", self.cars_to_recent_bboxes_map[carnumber])
                if len(self.cars_to_recent_bboxes_map[carnumber]):
                    bbox = np.average(self.cars_to_recent_bboxes_map[carnumber], axis = 0).astype(np.int32)
                    #print("after averaging:", self.cars_to_recent_bboxes_map[carnumber])
                    bbox = tuple(map(tuple, bbox))
                    bboxes_carnumbers.append((bbox, carnumber))
        
        return bboxes_carnumbers